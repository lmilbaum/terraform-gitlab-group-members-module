data "gitlab_group" "parent" {
  full_path = var.parent_full_path
}

data "gitlab_user" "user" {
  count = length(var.group_members)

  username = var.group_members[count.index].username
}

resource "gitlab_group_membership" "group_membership" {
  count = length(var.group_members)

  group_id     = data.gitlab_group.parent.group_id
  user_id      = data.gitlab_user.user[count.index].user_id
  access_level = data.gitlab_user.user[count.index].access_level
}
